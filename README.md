
# CXF JAX-RS Enablement for Alfresco Content Services

This is an [Alfresco Content Services](https://www.alfresco.com) module that provides the missing components to support JAX-RS-based clients.  There is no code, automation, or anything fancy provided by this module.  It just includes the necessary libraries.

## Install

You can install this like any Alfresco Module Package (AMP).  See the [Alfresco Documentation](https://docs.alfresco.com) for details.  Be sure to use the specific major/minor version of this built library when installation in your ACS platform.  For instance, `acs72` should be used with ACS v7.2.x.

## Configuration

There is nothing to configure.
